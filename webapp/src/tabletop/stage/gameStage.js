import {HUD} from "../hud/hud.js";
import {Grid} from "../grid.js";

export class GameStage extends PIXI.Container {
    HUD;
    grid;

    constructor() {
        super();

        this.grid = new Grid();
        this.addChild(this.grid);

        this.HUD = new HUD();
        this.addChild(this.HUD);
    }
}