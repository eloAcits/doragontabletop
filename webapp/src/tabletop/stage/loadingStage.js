import {HudButton} from "../hud/hudButton.js";
import {EVENTS} from "../const/events.js";
import {app} from "../application.js";
import {LoadingBar} from "../common/loadingBar.js";
import {TEXTURES} from "../const/textures.js";

export class LoadingStage extends PIXI.Container {

    loadingBar;

    constructor() {
        super();

        let text = new PIXI.Text('Loading Resources',
            {fontFamily : 'Arial', fontSize: 24, fill : 0xFFFFFF, align : 'center'}
        );

        let centerX = innerWidth / 2 - (text.width / 2);
        let centerY = innerHeight / 2 - (text.height / 2);
        text.position.set(centerX, centerY);
        this.addChild(text);

        this.loadingBar = new LoadingBar(innerWidth / 2 - 150, innerHeight / 2 + 20, 300, 30);
        this.addChild(this.loadingBar);

        this.loadTextures();
    }

    loadTextures(){
        let loader = PIXI.Loader.shared;

        loader
            .add("HUD_bar", "assets/menu_bar.png")
            .add("HUD_map", "assets/map.png")
            .add("HUD_button", "assets/menu_button.png")
            .add("HUD_button_over", "assets/menu_button_over.png")
            .add("HUD_menu_images", "assets/menu_images.png")
            .add("HUD_menu_plus", "assets/menu_plus.png")
            .add("HUD_add_images_button", "assets/add_images_button.png")
            .add("HUD_add_images_button_over", "assets/add_images_button_over.png")
            .add("HUD_image_preview_border", "assets/image_preview_border.png");

        loader.onProgress.add((event) => {
            this.loadingBar.setProgress(event.progress / 100);
        });

        loader.load((loader, resources) => {
            TEXTURES.HUD.bar = resources.HUD_bar.texture;
            TEXTURES.HUD.addImagesIcon = resources.HUD_map.texture;
            TEXTURES.HUD.addImagesMenu = resources.HUD_menu_images.texture;
            TEXTURES.HUD.plus = resources.HUD_menu_plus.texture;
            TEXTURES.HUD.addImagesButton = resources.HUD_add_images_button.texture;
            TEXTURES.HUD.addImagesButtonOver = resources.HUD_add_images_button_over.texture;
            TEXTURES.HUD.imagePreviewBorder = resources.HUD_image_preview_border.texture;

            HudButton.normalTexture = resources.HUD_button.texture;
            HudButton.mouseoverTexture = resources.HUD_button_over.texture;
        });

        loader.onComplete.add(() => {
            const event = new CustomEvent(EVENTS.RESOURCES_LOADING_COMPLETE);
            document.dispatchEvent(event);
        });
    }
}