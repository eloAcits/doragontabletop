import {app} from './application.js'
import {MAX_CELL_SIZE, MIN_CELL_SIZE} from "./const/constant.js";

export class Grid extends PIXI.Container {

    lines;

    // grid config
    thickness = 1;
    color = 0xF0F0FF;
    alpha = 0.5;
    cellSize = MIN_CELL_SIZE;

    zoomSpeed = 10;

    gridPosition = null;
    gridOffset = {
        x: 0,
        y: 0
    };
    dragging = false;

    constructor() {
        super();
        this.init();

        this.createLines();

        window.addEventListener("resize", () => {
            this.createLines();
        });
    }

    createLines() {
        if (this.lines != null) {
            this.lines.destroy(true);
        }
        this.lines = new PIXI.Graphics();
        this.drawLines();

        this.lines._zIndex = -1;
        this.lines.position.set(-this.cellSize, -this.cellSize);

        this.addChild(this.lines);
    }

    drawLines() {
        this.lines.lineStyle({
            width: this.thickness,
            color: this.color,
            alpha: this.alpha
        });

        // vertical
        let lineCount = window.screen.width * window.devicePixelRatio / MIN_CELL_SIZE;
        for (let i = 0; i <= lineCount + 2; i++) {
            this.lines.moveTo(this.cellSize * i, 0);
            this.lines.lineTo(this.cellSize * i, innerHeight + this.cellSize * 2);
        }
        // horizontal
        lineCount = innerHeight * window.devicePixelRatio / MIN_CELL_SIZE;
        for (let i = 0; i <= lineCount + 2; i++) {
            this.lines.moveTo(0, this.cellSize * i);
            this.lines.lineTo(innerWidth + this.cellSize * 2, this.cellSize * i);
        }
    }

    init() {
        this.interactive = true;
        this.interactiveChildren = true;
        this.hitArea = new PIXI.Rectangle(0, 0, 100000, 100000);

        document.addEventListener("wheel", (event) => {
            let zoom = event.deltaY / Math.abs(event.deltaY);
            let oldCellSize = this.cellSize;
            this.cellSize += (zoom * this.zoomSpeed);

            if (this.cellSize > MAX_CELL_SIZE || this.cellSize < MIN_CELL_SIZE) {
                this.cellSize = oldCellSize;
            } else {
                this.gridOffset.x = (this.gridOffset.x % this.cellSize) - this.cellSize;
                this.gridOffset.y %= this.cellSize;
            }
            this.createLines();
        });

        document.addEventListener("mousedown", (event) => {
            if (event.button === 1) {
                document.body.style.cursor = "pointer";
                this.dragging = true;
            }
        });

        document.addEventListener("mouseup", (event) => {
            if (event.button === 1) {
                document.body.style.cursor = "default";
                this.gridPosition = null;
                this.dragging = false;
            }
        });

        this.mousemove = (event) => {
            if (this.dragging) {
                let position = event.data.global;

                if (this.gridPosition != null) {
                    let x = (position.x - this.gridPosition.x);
                    let y = (position.y - this.gridPosition.y);
                    this.gridOffset.x = x;
                    this.gridOffset.y = y;
                } else {
                    this.gridPosition = {x: 0, y: 0};
                }

                this.gridPosition.x = position.x;
                this.gridPosition.y = position.y;

                this.lines.position.x += this.gridOffset.x;
                this.lines.position.y += this.gridOffset.y;

                this.lines.position.x = (this.lines.x % this.cellSize) - this.cellSize;
                this.lines.position.y = (this.lines.y % this.cellSize) - this.cellSize;
            }
        };
    }
}