import {TEXTURES} from "../const/textures.js";
import {EVENTS} from "../const/events.js";
import {Button} from "../common/button.js";
import {HudButton} from "./hudButton.js";
import {ImagePreview} from "./imagePreview.js";
import {ImageList} from "./imageList.js";

export class ImagesMenu extends PIXI.Container {

    backGround;
    addImageButton;
    imageList;

    constructor() {
        super();
        this.backGround = new PIXI.Sprite(TEXTURES.HUD.addImagesMenu);
        this.backGround.zIndex = 1;

        this.addChild(this.backGround);
        this.backGround.position.set(65, 10);
        this.visible = false;

        document.getElementById("filePicker").onchange = this.addImage;
        document.addEventListener(EVENTS.UPLOAD_IMAGE, () => {
            document.getElementById("filePicker").click();
        })
        this.addImageButton = new Button(
            null,
            EVENTS.UPLOAD_IMAGE,
            null,
            TEXTURES.HUD.addImagesButton,
            TEXTURES.HUD.addImagesButtonOver
        );
        this.addImageButton.scale.set(0.4);
        this.addImageButton.position.set(70, 10);
        this.addChild(this.addImageButton);

        this.imageList = new ImageList();
        this.imageList.position.set(75, 70);
        this.addChild(this.imageList);
    }

    loadImages() {
        fetch("http://localhost:8080/images/available")
            .then(res => res.json())
            .then((data) => {
                data.forEach(it => {
                    if (!this.imageList.hasPreviewImage(it.id)) {
                        this.imageList.addImagePreview(it);
                    }
                });
            })
    }

    addImage() {
        let form = document.getElementById("fileForm");

        const XHR = new XMLHttpRequest();
        const FD = new FormData(form);

        XHR.open("POST", "http://localhost:8080/images/add");
        XHR.send(FD);

        this.loadImages();
    }
}