import {ImagePreview} from "./imagePreview.js";

export class ImageList extends PIXI.Container {

    previews = [];

    constructor() {
        super();
    }

    addImagePreview(imageData){
        let currentCount = this.previews.length;

        let imagePreview = new ImagePreview(imageData.imageName, imageData.id);
        imagePreview.position.set(0, 160 * currentCount);
        this.previews.push(imagePreview);
        this.addChild(imagePreview);
    }

    hasPreviewImage(id){
        for(let i = 0; i < this.previews.length; i++){
            if(this.previews[i].imageId === id){
                return true
            }
        }
        return false;
    }
}