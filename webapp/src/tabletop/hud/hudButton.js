import {Button} from "../common/button.js";

export class HudButton extends Button {

    static normalTexture;
    static mouseoverTexture;

    constructor(icon, eventName, payload) {
        super(icon, eventName, payload, HudButton.normalTexture, HudButton.mouseoverTexture);
    }
}