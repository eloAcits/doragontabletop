import {app} from '../application.js'
import {HudButton} from "./hudButton.js";
import {ImagesMenu} from "./imagesMenu.js";
import {EVENTS} from "../const/events.js";
import {TEXTURES} from "../const/textures.js";

export class HUD extends PIXI.Container{
    buttonTextures = {};
    sprites = {};

    buttons = {};
    menus = {};

    leftBarButtons = {};

    constructor() {
        super();
        this.init();
    }

    init(){
        this.sprites = {};
        this.sprites.HUD_bar = new PIXI.Sprite(TEXTURES.HUD.bar);
        this.sprites.HUD_bar.position.set(10, 10);
        this.addChild(this.sprites.HUD_bar);

        this.menus.addImages = new ImagesMenu();
        this.addChild(this.menus.addImages);

        document.addEventListener(EVENTS.OPEN_IMAGES_MENU, this.openImagesMenu);
        this.leftBarButtons.addImages = new HudButton(
            TEXTURES.HUD.addImagesIcon,
            EVENTS.OPEN_IMAGES_MENU,
            this.menus.addImages
        );
        this.leftBarButtons.addImages.position.set(13, 35);
        this.addChild(this.leftBarButtons.addImages);
    }

    openImagesMenu(e){
        let imagesMenu = e.detail;
        imagesMenu.loadImages();
        imagesMenu.visible = !imagesMenu.visible;
    }
}