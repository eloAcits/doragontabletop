import {TEXTURES} from "../const/textures.js";

export class ImagePreview extends PIXI.Container {

    background;
    thumbnail;
    imageTitle;
    imageId;

    constructor(imageTitle, imageId) {
        super();

        this.imageId = imageId;

        this.background = new PIXI.Sprite(TEXTURES.HUD.imagePreviewBorder);
        //this.addChild(this.background);

        this.imageTitle = new PIXI.Text(imageTitle,
            {fontFamily : 'Arial', fontSize: 16, fill : 0xFFFFFF, align : 'center'}
        );
        this.imageTitle.position.set(0, -20);
        this.addChild(this.imageTitle);

        this.thumbnail = PIXI.Sprite.from("http://localhost:8080/images/thumbnail/" + imageId);
        this.addChild(this.thumbnail);
    }

}