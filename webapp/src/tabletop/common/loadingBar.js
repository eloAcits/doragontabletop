export class LoadingBar extends PIXI.Container {
    backgroundBar;
    foregroundBar;
    loadingInfoText;

    constructor(x, y, width, height) {
        super();

        this.position.set(x, y);

        this.backgroundBar = new PIXI.Graphics();
        this.backgroundBar.beginFill(0xFFFFFF, 1);
        this.backgroundBar.drawRect(0, 0, width, height);
        this.addChild(this.backgroundBar);

        this.foregroundBar = new PIXI.Graphics();
        this.foregroundBar.beginFill(0xFF0000, 1);
        this.foregroundBar.drawRect(2, 2, width - 4, height - 4);
        this.addChild(this.foregroundBar);

        this.loadingInfoText = new PIXI.Text('',
            {fontFamily : 'Arial', fontSize: 18, fill : 0xFFFFFF, align : 'center'}
        );
        this.loadingInfoText.position.set(0, height + 5);
        this.addChild(this.loadingInfoText);
    }

    setProgress(progress){
        this.foregroundBar.scale.x = progress;
    }

    setInfo(text){
        this.loadingInfoText.setText(text);
    }
}