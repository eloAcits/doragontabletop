export class Button extends PIXI.Sprite{

    constructor(icon, eventName, payload, normalTexture, mouseoverTexture) {
        super();

        this.interactive = true;
        this.iconTexture = icon;
        this.normalTexture = normalTexture;
        this.action = eventName;
        this.mouseoverTexture = mouseoverTexture;

        this.texture = normalTexture;

        this.on('mouseover', () => {
            this.texture = this.mouseoverTexture;
        });

        this.on('mouseout', () => {
            this.texture = this.normalTexture;
        });

        const event = new CustomEvent(eventName, {detail: payload});
        this.on('click', () => {
            document.dispatchEvent(event);
        });

        this.iconSprite = new PIXI.Sprite(this.iconTexture);
        this.iconSprite.scale.x = 0.25;
        this.iconSprite.scale.y = 0.25;
        this.iconSprite.x = 7;
        this.iconSprite.y = 7;
        this.addChild(this.iconSprite);
    }
}