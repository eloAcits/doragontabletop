export let TEXTURES = {
    HUD:
        {
            bar: null,
            addImagesIcon: null,
            addImagesMenu: null,
            plus: null,
            imagePreviewBorder: null,
        }
};
