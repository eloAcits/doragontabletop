export let EVENTS = {
    RESOURCES_LOADING_COMPLETE: "resourcesLoadingComplete",
    OPEN_IMAGES_MENU: "openImagesMenuEvent",
    UPLOAD_IMAGE: "uploadImageEvent"
}
