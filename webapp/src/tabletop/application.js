import {LoadingStage} from "./stage/loadingStage.js";
import {GameStage} from "./stage/gameStage.js";
import {EVENTS} from "./const/events.js";

export let app = new PIXI.Application(
    {
        width: 256,
        height: 256,
        antialias: true,
        resolution: 1
    }
);

app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoDensity = false;
app.renderer.resize(window.innerWidth, window.innerHeight);
app.stage.sortableChildren = true;

app.stage = new LoadingStage();

document.addEventListener(EVENTS.RESOURCES_LOADING_COMPLETE, () => {
    app.stage = new GameStage();
});

document.oncontextmenu = () => {
    return false;
}

window.addEventListener("resize", function (event){
    app.renderer.resize(window.innerWidth, window.innerHeight);
})