package pl.acits.doragonrest.services;

import org.springframework.stereotype.Service;
import pl.acits.doragonrest.model.UploadedImage;
import pl.acits.doragonrest.model.views.Image;
import pl.acits.doragonrest.model.views.ImagePreview;
import pl.acits.doragonrest.model.views.ImageThumbnail;
import pl.acits.doragonrest.repositories.UploadedImageRepository;

import java.util.List;

@Service
public class UploadedImageService {
    //public static final String FIND_AVAILABLE = "SELECT id, image_name, thumbnail FROM uploaded_image";
    public final UploadedImageRepository uploadedImageRepository;

    public UploadedImageService(UploadedImageRepository uploadedImageRepository) {
        this.uploadedImageRepository = uploadedImageRepository;
    }

    public List<UploadedImage> list(){
        return uploadedImageRepository.findAll();
    }

    public void insert(UploadedImage image){
        uploadedImageRepository.save(image);
    }

    public List<ImagePreview> listAvailable() {
        return uploadedImageRepository.findAllImagePreviewsBy();
    }

    public Image getImage(long id){
        return uploadedImageRepository.getImageById(id);
    }

    public ImageThumbnail getThumbnail(long id){
        return uploadedImageRepository.findThumbnailById(id);
    }
}
