package pl.acits.doragonrest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.acits.doragonrest.common.ImageUtils;
import pl.acits.doragonrest.model.UploadedImage;
import pl.acits.doragonrest.services.UploadedImageService;

import java.io.IOException;
import java.util.List;

@RestController
public class ImagesController {

    Logger logger = LoggerFactory.getLogger(ImagesController.class);

    private final UploadedImageService uploadedImageService;

    public ImagesController(UploadedImageService uploadedImageService) {
        this.uploadedImageService = uploadedImageService;
    }

    @GetMapping("/images/available")
    public List getAvailableImages() {
        return this.uploadedImageService.listAvailable();
    }

    @CrossOrigin(origins = "http://localhost")
    @PostMapping(value = "/images/add")
    public void uploadImage(@RequestParam("image") MultipartFile image) throws IOException {
        UploadedImage uploadedImage = new UploadedImage();
        uploadedImage.id = 0L;

        uploadedImage.image = image.getBytes();
        uploadedImage.imageName = image.getOriginalFilename();
        uploadedImage.thumbnail = ImageUtils.createThumbnail(image.getBytes());

        uploadedImageService.insert(uploadedImage);
    }

    @GetMapping(
            produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE},
            path =  "/images/get/{id}"
    )
    public byte[] getImage(@PathVariable long id) {
        return uploadedImageService.getImage(id).getImage();
    }

    @GetMapping(
            produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE},
            path =  "/images/thumbnail/{id}"
    )
    public byte[] getThumbnail(@PathVariable long id) {
        return uploadedImageService.getThumbnail(id).getThumbnail();
    }
}
