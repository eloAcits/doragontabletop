package pl.acits.doragonrest.common;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUtils {

    public static byte[] createThumbnail(byte[] origin) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(origin);
        BufferedImage originImage = ImageIO.read(inputStream);
        int originWidth = originImage.getWidth();
        int originHeight = originImage.getHeight();

        int resultWidth;
        int resultHeight;

        if(originWidth > originHeight){
            resultWidth = 128;
            resultHeight = originHeight / (originWidth / 128);
        } else {
            resultWidth = originWidth / (originHeight / 128);
            resultHeight = 128;
        }

        Image resultingImage = originImage.getScaledInstance(resultWidth, resultHeight, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage(resultWidth, resultHeight, BufferedImage.TYPE_INT_RGB);
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(outputImage, "jpg", outputStream);
        return outputStream.toByteArray();
    }
}
