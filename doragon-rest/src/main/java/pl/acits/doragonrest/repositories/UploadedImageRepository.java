package pl.acits.doragonrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.acits.doragonrest.model.UploadedImage;
import pl.acits.doragonrest.model.views.Image;
import pl.acits.doragonrest.model.views.ImagePreview;
import pl.acits.doragonrest.model.views.ImageThumbnail;

import java.util.List;

@Repository
public interface UploadedImageRepository extends JpaRepository<UploadedImage, Long> {

    List<ImagePreview> findAllImagePreviewsBy();

    Image getImageById(long id);

    ImageThumbnail findThumbnailById(long id);
}
