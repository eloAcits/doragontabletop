package pl.acits.doragonrest.model.views;

public interface ImageThumbnail {
    byte[] getThumbnail();
}
