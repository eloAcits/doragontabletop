package pl.acits.doragonrest.model.views;

public interface ImagePreview {
    long getId();
    String getImageName();
}
