package pl.acits.doragonrest.model;

import javax.persistence.*;

@Entity
public class UploadedImage {

    @Id
    @GeneratedValue
    public Long id;

    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    public byte[] image;

    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    public byte[] thumbnail;

    public String imageName;
}
