package pl.acits.doragonrest.model.views;

public interface Image {
    long getId();

    byte[] getImage();
}
